﻿

using Veayo24._PS.Client.Constants;

namespace Veayo24._PS.Client.Exceptions.Users
{
    public class LoginFailedException : BaseException
    {
        public override string Code => ExceptionCode.LoginFailed;
    }
}
