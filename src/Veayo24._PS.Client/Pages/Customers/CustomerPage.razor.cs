﻿using Microsoft.AspNetCore.Components;

namespace Veayo24._PS.Client.Pages.Customers
{
    public partial class CustomerPage
    {

        //inject
        [Inject] NavigationManager _navigationManager { get; set; }

        private void OnCreate()
        {
            _navigationManager.NavigateTo("/customer/create");
        }
    }
}
