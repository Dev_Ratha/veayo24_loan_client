﻿using AntDesign;
using Veayo24._PS.Client.Models.Customers;

namespace Veayo24._PS.Client.Pages.Customers
{
    public partial class CreateCustomerPage
    {
        public CustomerModel Model { get; set; } = new CustomerModel();
        Form<CustomerModel> _form;


        private async Task onCreateAsync()
        {
            _form.Submit();
        }
    }
}
