﻿using AntDesign;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using Veayo24._PS.Client.Models.Users;
using Veayo24._PS.Client.Services;
using Veayo24._PS.Client.Services.Interfaces;

namespace Veayo24._PS.Client.Pages.Users
{
    public partial class ViewUserPage
    {
        [Parameter] public long Id { get; set; }
        public bool Loading { get; set; } = true;
        public UserModel Model { get; set; }
        public bool ModalVisible { get; set; }
        public IEnumerable<UserMenuNestedModel> Menus { get; set; } = new UserMenuNestedModel[] { };
        public bool SaveMenuLoading { get; set; }
        public bool MenuTableLoading { get; set; }
        //inject
        [Inject] IUserService _userService { get; set; }
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] ModalService _modalService { get; set; }
        [Inject] HttpInterceptorService _httpInterceptorService { get; set; }
        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();
            var tuser = LoadUserAsync();
            var tuserMenu = LoadUserMenu();
            await tuser;
            await tuserMenu;
        }
        private async Task LoadUserAsync()
        {
            if (Id > 0)
            {
                try
                {
                    Loading = true;

                    _httpInterceptorService.IsSilent();
                    Model = await _userService.GetUserByIdAsync(Id);
                    Loading = false;
                }
                catch
                {
                    _navigationManager.NavigateTo("/resource-not-found");

                    Loading = false;

                }

            }
        }

        private async Task LoadUserMenu()
        {
            try
            {

                Menus = new UserMenuNestedModel[] { };
                MenuTableLoading = true;
                StateHasChanged();
                var menu = await _userService.GetUserMenuAsync(Id);
                Menus = menu.AsNestedEnumerable();
                MenuTableLoading = false;
            }
            catch
            {
                MenuTableLoading = false;
            }
        }

        private async Task OnButtonSaveRoleAndMenu()
        {
            try
            {
                SaveMenuLoading = true;
                await _userService.SaveUserMenuAsync(Id, Menus);
                SaveMenuLoading = false;
                await LoadUserMenu();
            }
            catch
            {
                SaveMenuLoading = false;
            }
        }

        private void OnEditUser()
        {
            _navigationManager.NavigateTo($"/user/{Id}");
        }

        private async Task OnRemove()
        {
            var option = new ConfirmOptions
            {
                Title = "Remove",
                Content = "Are you sure you want to remove?",
                OkType = "danger",
                OkText = "Remove",
                OnOk = async e =>
                {
                    await _userService.DeleteUserAsync(Id);
                    _navigationManager.NavigateTo("/user");
                }
            };
            await _modalService.ConfirmAsync(option);
        }

    }
}
