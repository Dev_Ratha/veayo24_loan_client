﻿using AntDesign;
using AntDesign.TableModels;
using Microsoft.AspNetCore.Components;
using Veayo24._PS.Client.Models.Pagination;
using Veayo24._PS.Client.Models.Users;
using Veayo24._PS.Client.Resources;
using Veayo24._PS.Client.Services.Interfaces;

namespace Veayo24._PS.Client.Pages.Users
{
    public partial class UserPage
    {
        public Pagination<UserModel> PageData { get; set; } = Pagination<UserModel>.Empty();
        public PageDto Page { get; set; } = PageDto.Empty();
        public bool ListLoading { get; set; }
        public bool ModalVisible { get; set; }
        public bool ModalConfirmLoading { get; set; }
        public UserModel FormModel { get; set; } = new UserModel();
        public bool ModalLoading { get; set; }
        //inject
        [Inject] IUserService _userService { get; set; }
        [Inject] ModalService _modalService { get; set; }
        [Inject] NavigationManager _navigation { get; set; }
        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();
            await LoadPageAsync();
        }

        public void OnTableRowClick(UserModel user)
        {
            _navigation.NavigateTo($"user/{user.Id}");
        }

        private async Task LoadPageAsync()
        {
            try
            {
                ListLoading = true;
                PageData = await _userService.GetUserPage(Page);
                Page.Update(PageData);
                ListLoading = false;
            }
            catch
            {
                ListLoading = false;
            }

        }
        private async Task SearchAsync(string search)
        {
            Page.Search = search;
            await LoadPageAsync();
        }
        private async Task RowRemoveAsync(UserModel user)
        {
            var options = new ConfirmOptions
            {
                Title = AppResource.ModalRemoveTitle,
                Content = AppResource.ModalRemoveContent,
                OkType = "danger",
                OkText = AppResource.ButtonRemove,
                OnOk = async e =>
                {
                    Console.WriteLine("sefs");
                    await _userService.DeleteUserAsync(user.Id ?? 0);
                    await LoadPageAsync();
                }
            };
            await _modalService.ConfirmAsync(options);
        }
        private async Task OnCreateSubmit(UserModel model)
        {
            try
            {
                ModalConfirmLoading = true;
                await _userService.CreateUserAsync(model);
                ModalVisible = false;
                ModalConfirmLoading = false;
                await LoadPageAsync();
            }
            catch
            {
                ModalConfirmLoading = false;
            }
        }
        private async Task OnUpdateSubmit(UserModel model)
        {
            try
            {
                ModalConfirmLoading = true;
                await _userService.UpdateUserAsync(model);
                ModalVisible = false;
                ModalConfirmLoading = false;
                await LoadPageAsync();
            }
            catch
            {
                ModalConfirmLoading = false;
            }
        }
        private async Task RowEditAsync(UserModel model)
        {
            try
            {
                ModalLoading = true;
                ModalVisible = true;
                FormModel = await _userService.GetUserByIdAsync(model.Id ?? 0);
                FormModel.Password = "************";

                ModalLoading = false;
            }
            catch
            {

            }
        }
    }
}
