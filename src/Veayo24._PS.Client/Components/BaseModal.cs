﻿using System.Threading.Tasks;
using AntDesign;
using Microsoft.AspNetCore.Components;

namespace Veayo24._PS.Client.Components;

public class BaseModal : ComponentBase
{
    [Parameter]
    public bool Loading { get; set; } = false;
    [Parameter]
    public bool VisibleValue { get; set; } = false;
    [Parameter]
    public bool ConfirmLoading { get; set; } = false;
    [Parameter]
    public EventCallback<bool> VisibleValueChanged { get; set; }

    [Parameter]
    public EventCallback<bool> OnLoadingChanged { get; set; }

    public async Task SetLoading(bool val)
    {
        Loading = val;
        await OnLoadingChanged.InvokeAsync(Loading);
        StateHasChanged();
    }
    public async Task SetVisible(bool val)
    {
        VisibleValue = val;
        await VisibleValueChanged.InvokeAsync(val);
        StateHasChanged();
    }

    public async Task OnCancel()
    {
        VisibleValue = false;
        await VisibleValueChanged.InvokeAsync(false);
        await OnClear();
    }
    public virtual async Task OnClear()
    {

    }

}
