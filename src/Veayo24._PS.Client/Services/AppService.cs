﻿using System.Net.Http.Json;
using Veayo24._PS.Client.Models.Apps;
using Veayo24._PS.Client.Services.Interfaces;

namespace Veayo24._PS.Client.Services
{
    public class AppService : IAppService
    {
        private readonly HttpClient _http;

        public AppService(HttpClient http)
        {
            _http = http;
        }
        public Task<IEnumerable<AppMenuModel>> GetAppMenuAsync()
            => _http.GetFromJsonAsync<IEnumerable<AppMenuModel>>($"/v1/app/menu");
    }
}
