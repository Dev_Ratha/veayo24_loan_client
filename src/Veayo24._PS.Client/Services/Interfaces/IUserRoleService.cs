﻿using SchoolClient.Services.Interfaces;
using Veayo24._PS.Client.Models.UserRoles;

namespace Veayo24._PS.Client.Services.Interfaces
{
    public interface IUserRoleService : IService
    {
        Task<IEnumerable<UserRoleModel>> GetListAsync();
    }
}
