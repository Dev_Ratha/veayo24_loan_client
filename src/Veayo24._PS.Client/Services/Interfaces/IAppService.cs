﻿using SchoolClient.Services.Interfaces;
using Veayo24._PS.Client.Models.Apps;

namespace Veayo24._PS.Client.Services.Interfaces
{
    public interface IAppService : IService
    {
        Task<IEnumerable<AppMenuModel>> GetAppMenuAsync();
    }
}
