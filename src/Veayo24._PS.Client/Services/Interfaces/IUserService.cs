﻿using SchoolClient.Models.Logins;
using SchoolClient.Services.Interfaces;
using Veayo24._PS.Client.Models.ChangePasswords;
using Veayo24._PS.Client.Models.Logins;
using Veayo24._PS.Client.Models.Pagination;
using Veayo24._PS.Client.Models.Users;

namespace Veayo24._PS.Client.Services.Interfaces
{
    public interface IUserService : IService
    {
        Task<JsonWebToken> LoginAsync(LoginFormModel model);
        Task LogoutAsync();
        Task SaveToken(string token);
        Task ClearToken();
        Task<string> GetToken();
        Task CheckUserTokenAsync();
        Task<Pagination<UserModel>> GetUserPage(PageDto page);
        Task CreateUserAsync(UserModel user);
        Task UpdateUserAsync(UserModel user);
        Task DeleteUserAsync(long id);
        Task<UserModel> GetUserByIdAsync(long id);
        Task ChangePasswordAsync(long userId, ChangePasswordModel model);
        Task<IEnumerable<UserMenuModel>> GetUserMenuAsync(long userId);
        Task SaveUserMenuAsync(long userId, IEnumerable<UserMenuNestedModel> models);
    }
}
