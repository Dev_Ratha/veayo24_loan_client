﻿
using AntDesign.ProLayout;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using Veayo24._PS.Client.Models.Apps;
using Veayo24._PS.Client.Services;
using Veayo24._PS.Client.Services.Interfaces;

namespace Veayo24._PS.Client.Shared
{
    public partial class MainLayout : IDisposable
    {
        public bool Loading { get; set; } = true;

        public IEnumerable<MenuDataItem> MenuData { get; set; } = new MenuDataItem[] { };
        //[Inject] public INavMenuService _service { get; set; }

        [Inject] public HttpInterceptorService _interceptor { get; set; }
        [Inject] public IAppService _appService { get; set; }


        protected override async Task OnInitializedAsync()
        {
            _interceptor.MonitorEvent();
            token = await _userService.GetToken();
            IsLogin = !string.IsNullOrEmpty(token);

            await LoadMenuAsync();
        }



        private async Task LoadMenuAsync()
        {
            try
            {
                var menus = await _appService.GetAppMenuAsync();
                MenuData = menus.AsNestedEnumerable();
            }
            catch
            {

            }

        }
        public void Dispose()
        {
            _interceptor.DisposeEvent();
        }
    }
}
