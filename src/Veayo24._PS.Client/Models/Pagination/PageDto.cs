﻿namespace Veayo24._PS.Client.Models.Pagination
{
    public class PageDto
    {
        public int PageId { get; set; } = 1;
        public int Result { get; set; } = 10;
        public string Search { get; set; } = null;
        public string Sort { get; set; } = null;
        public string GetUrl() => $"pageId={PageId}&result={Result}&search={Search}&sort={Sort}";

        public static PageDto Empty()
            => new PageDto();

        public void Update<T>(Pagination<T> page)
        {
            PageId = page.CurrentPage;
            Result = page.Result;
        }
    }
}
