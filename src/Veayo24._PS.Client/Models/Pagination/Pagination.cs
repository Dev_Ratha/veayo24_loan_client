﻿using System.Collections.Generic;

namespace Veayo24._PS.Client.Models.Pagination
{
    public class Pagination<T>
    {
        public int TotalPageCount { get; set; }
        public int ItemCount { get; set; }
        public int AllItemCount { get; set; }
        public int CurrentPage { get; set; } = 1;
        public int Result { get; set; } = 10;
        public bool HasNextPage { get; set; }
        public bool HasPreviousPage { get; set; }
        public IEnumerable<T> DataList { get; set; }

        public static Pagination<T> Empty()
            => new Pagination<T>() { DataList = new T[] { } };
    }
}
