﻿namespace Veayo24._PS.Client.Models.Apps
{
    public class AppMenuModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Icon { get; set; }
        public long? ParentId { get; set; }
        public bool IsShow { get; set; }
        public bool IsView { get; set; }
        public bool IsAdd { get; set; }
        public bool IsUpdate { get; set; }
        public bool IsDelete { get; set; }
    }
}
