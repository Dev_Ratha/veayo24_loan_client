﻿using AntDesign.ProLayout;

namespace Veayo24._PS.Client.Models.Apps
{
    public static class Extensions
    {
        public static MenuDataItem AsNested(this AppMenuModel x)
            => new MenuDataItem()
            {
                Key = x.Id.ToString(),
                Name = x.Name,
                Children = null,
                Icon = x.Icon,
                Path = x.Path
            };
        public static IEnumerable<MenuDataItem> AsNestedEnumerable(this IEnumerable<AppMenuModel> navMenus)
        {
            var result = new HashSet<MenuDataItem>();
            var mains = navMenus.Where(x => (x.ParentId ?? 0) == 0);
            foreach (var navMenu in mains)
            {
                var menu = navMenu.AsNested();
                result.Add(menu);
                if (navMenus.Any(x => x.ParentId == navMenu.Id))
                {
                    var ch = RecursiveMenu((int)navMenu.Id, navMenus);
                    menu.Children = ch.ToArray();
                }
            }
            return result;
        }
        private static IEnumerable<MenuDataItem> RecursiveMenu(int parentId, IEnumerable<AppMenuModel> navMenus)
        {
            var result = new HashSet<MenuDataItem>();
            var mains = navMenus.Where(x => x.ParentId == parentId);
            foreach (var navMenu in mains)
            {
                var menu = navMenu.AsNested();
                result.Add(menu);
                if (navMenus.Any(x => x.ParentId == navMenu.Id))
                {
                    var ch = RecursiveMenu((int)navMenu.Id, navMenus);
                    menu.Children = ch.ToArray();
                }
            }
            return result;
        }
    }
}
