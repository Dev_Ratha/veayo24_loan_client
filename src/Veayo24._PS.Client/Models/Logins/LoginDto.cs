﻿namespace SchoolClient.Models.Logins
{
    public class LoginDto
    {
        public string AccessToken { get; set; }
    }
}
