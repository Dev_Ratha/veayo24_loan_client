﻿using System.ComponentModel.DataAnnotations;

namespace Veayo24._PS.Client.Models.Customers
{
    public class CustomerModel
    {
        public long? Id { get; set; }
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        public string NameLatin { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        public DateTime? DOB { get; set; }
        public string Address { get; set; }
        public string IdNumber { get; set; }
        public string MaritalStatus { get; set; }

    }
}
