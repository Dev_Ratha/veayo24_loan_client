﻿namespace Veayo24._PS.Client.Models.UserRoles
{
    public class UserRoleModel
    {
        public long? Id { get; set; }
        public string Role { get; set; }
    }
}
