﻿using System.ComponentModel.DataAnnotations;

namespace Veayo24._PS.Client.Models.ChangePasswords
{
    public class ChangePasswordModel
    {
        [Required]
        public string CurrentPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required]
        public string ConfirmPassword { get; set; }
    }
}
