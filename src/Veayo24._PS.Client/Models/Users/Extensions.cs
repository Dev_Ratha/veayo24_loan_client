﻿namespace Veayo24._PS.Client.Models.Users
{
    public static class Extensions
    {
        public static UserMenuNestedModel AsNested(this UserMenuModel x)
           => new UserMenuNestedModel()
           {
               Id = x.Id,
               Icon = x.Icon,
               Childs = new List<UserMenuNestedModel>(),
               IsAdd = x.IsAdd ?? false,
               IsDelete = x.IsDelete ?? false,
               IsShow = x.IsShow ?? false,
               IsUpdate = x.IsUpdate ?? false,
               IsView = x.IsView ?? false,
               Name = x.Name,
               ParentId = x.ParentId,
               Path = x.Path,
               UserId = x.UserId
           };

        public static IEnumerable<UserMenuNestedModel> AsNestedEnumerable(this IEnumerable<UserMenuModel> navMenus)
        {
            var result = new HashSet<UserMenuNestedModel>();
            var mains = navMenus.Where(x => (x.ParentId ?? 0) == 0);
            foreach (var navMenu in mains)
            {
                var menu = navMenu.AsNested();
                result.Add(menu);
                if (navMenus.Any(x => x.ParentId == navMenu.Id))
                {
                    var ch = RecursiveMenu(navMenu.Id, navMenus);
                    menu.Childs = ch.ToList();
                }
            }
            return result;
        }

        private static IEnumerable<UserMenuNestedModel> RecursiveMenu(long parentId, IEnumerable<UserMenuModel> navMenus)
        {
            var result = new HashSet<UserMenuNestedModel>();
            var mains = navMenus.Where(x => x.ParentId == parentId);
            foreach (var navMenu in mains)
            {
                var menu = navMenu.AsNested();
                result.Add(menu);
                if (navMenus.Any(x => x.ParentId == navMenu.Id))
                {
                    var ch = RecursiveMenu(navMenu.Id, navMenus);
                    menu.Childs = ch.ToList();
                }
            }
            return result;
        }
    }
}
