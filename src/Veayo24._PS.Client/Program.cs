﻿using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Toolbelt.Blazor.Extensions.DependencyInjection;
using Veayo24._PS.Client;
using Veayo24._PS.Client.Auths;
using Veayo24._PS.Client.Services;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp =>
{
    var client = new HttpClient { BaseAddress = new Uri("http://localhost:55777/"), Timeout = TimeSpan.FromSeconds(5) };
    client.EnableIntercept(sp);
    return client;
});
builder.Services.AddHttpClientInterceptor();
builder.Services.AddAntDesign();
builder.Services.AddServices();
builder.Services.AddLocalStorage();
builder.Services.AddAuthorizationCore();

builder.Services.AddScoped<AuthenticationStateProvider, ApiAuthenticationStateProvider>();
builder.Services.AddScoped<HttpInterceptorService>();
await builder.Build().RunAsync();
